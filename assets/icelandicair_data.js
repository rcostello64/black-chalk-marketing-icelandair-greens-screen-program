/**
 * Created by ryancostello on 8/29/16.
 */

var flag = false;
var $submit = $('#submit'), $button = $('button'), $alert_message = $('#alert_message'), $openRules = $('#openRules'), $rules = $('#rules'), $closeRules = $('#closeRules');
var alertTimer;

window.onload = function() {

    //intro animations
    $('#wrapper').velocity({ opacity: 1 }, {duration:1000});
};

function pageLoad() {
    $mz.onload();
}


function ReportResult() {
    if (validateFormOnSubmit()) {
        SubmitResult(this.document, 'mozeus');
    }
}

function validateFormOnSubmit() {

    var fname = $mz.validate.element($mz.find("FName"));
    var lname = $mz.validate.element($mz.find("LName"));
    var email = $mz.validate.element($mz.find("Email"));
    var agree = true;

    if (!$('#AgreeToRules').is(':checked')) {

        $alert_message.velocity('transition.expandIn');

        alertTimer = setTimeout(function() {
            clearTimeout(alertTimer);
            $alert_message.velocity('transition.expandOut');
        }, 3000)
    }

    return fname && lname && email && agree
}

$button.on('touchstart', function(e) {

    $(e.target).velocity({
        scale: "80%"
    }, {duration:200});

});

$button.on('touchend', function(e) {

    $(e.target).velocity({
        scale: "100%"
    }, {duration:200});

});

$submit.on('touchend', function(e) {

    e.stopPropagation();
    e.preventDefault();

    if (!flag) {

        flag = true;

        ReportResult();

        var flagTimer = setTimeout(function() {

            clearTimeout(flagTimer);
            flag = false

        }, 1000);


    }
});


$alert_message.on('touchend', function(e) {

    e.stopPropagation();
    e.preventDefault();

    if (!flag) {

        flag = true;

        clearTimeout(alertTimer);
        $alert_message.velocity('transition.expandOut');

        var flagTimer = setTimeout(function() {

            clearTimeout(flagTimer);
            flag = false

        }, 1000);


    }
});

$openRules.on('touchstart', function(e) {

    e.stopPropagation();
    e.preventDefault();

    if (!flag) {

        flag = true;

        $rules.velocity('transition.expandIn');

        var flagTimer = setTimeout(function() {

            clearTimeout(flagTimer);
            flag = false

        }, 1000);


    }
});


$('#closeRules').on('touchstart', function(e) {

    e.stopPropagation();
    e.preventDefault();

    if (!flag) {

        flag = true;

        $rules.velocity('transition.expandOut');

        var flagTimer = setTimeout(function() {

            clearTimeout(flagTimer);
            flag = false

        }, 1000);


    }
});



//function SubmitResultString(result) {alert(result)};

$(document).ready(function() {

    pageLoad();

});


