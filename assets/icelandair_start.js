/**
 * Created by ryancostello on 8/26/16.
 */

var flag = false;
var $logo_top = $('#logo_top'), $logo_bottom = $('#logo_bottom'), $get_started = $('#get_started'), $bg_thumb = $('.bg_thumb'), $select_bg = $('#select_bg'), $alert_message = $('#alert_message'), $button = $('button');
var background, alertTimer;

window.onload = function() {

    //intro animations
    $('#wrapper').velocity({ opacity: 1 }, {duration:1000});
    $logo_top.velocity("transition.slideLeftBigIn", {delay:1100});
    $logo_bottom.velocity("transition.slideRightBigIn", {delay:1300});
    $get_started.velocity("transition.slideUpBigIn", {delay:1500, display:'block'});

};

$get_started.on('touchend', function(e) {

    e.stopPropagation();
    e.preventDefault();

    if (!flag) {

        flag = true;

        showBgSelection(e);

        var flagTimer = setTimeout(function() {

            clearTimeout(flagTimer);
            flag = false

        }, 1000);


    }

});

function showBgSelection(e) {

    $(e.target).velocity({ scale: "100%"}, {duration:200, complete: function() {

        $logo_top.velocity("transition.slideLeftBigOut", {delay:900});
        $logo_bottom.velocity("transition.slideRightBigOut", {delay:700});
        $get_started.velocity("transition.slideDownBigOut", {delay:500,

            complete: function() {

                var $getThumbs = $('.bg_thumb');
                $getThumbs.velocity('transition.flipYIn', {delay:500});
                $('#select_bg').velocity("transition.slideUpBigIn", {delay:1500, display:'block'});

            }
        });
      }
    });

}

$button.on('touchstart', function(e) {

    $(e.target).velocity({
        scale: "80%"
    }, {duration:200});

});

$button.on('touchend', function(e) {

    $(e.target).velocity({
        scale: "100%"
    }, {duration:200});

});

$bg_thumb.on('touchstart', function(e) {

    $bg_thumb.css('boxShadow', 'none');

    $(e.target).velocity({
        scale: "95%"
    }, {duration:200});

});

$bg_thumb.on('touchend', function(e) {

    $(e.target).velocity({
        scale: "100%"
    }, {duration:200, complete: function() {
        $(e.target).css('boxShadow', '0 0 0 3px #ffb600');
        background = $(e.target).attr('data-bg-thumb');
    }});



});

$select_bg.on('touchend', function(e) {

    e.stopPropagation();
    e.preventDefault();

    if (!flag) {

        flag = true;

        valBgSelection();

        var flagTimer = setTimeout(function() {

            clearTimeout(flagTimer);
            flag = false

        }, 1000);


    }
});


$alert_message.on('touchend', function(e) {

    e.stopPropagation();
    e.preventDefault();

    if (!flag) {

        flag = true;

        clearTimeout(alertTimer);
        $alert_message.velocity('transition.expandOut');

        var flagTimer = setTimeout(function() {

            clearTimeout(flagTimer);
            flag = false

        }, 1000);


    }
});

function valBgSelection() {

    if (background === undefined) {

        $alert_message.velocity('transition.expandIn');

        alertTimer = setTimeout(function() {

            clearTimeout(alertTimer);
            $alert_message.velocity('transition.expandOut');

        }, 3000);

    } else {

        SubmitResultString('Background=' + background + ';');

    }

}

//function SubmitResultString(result) {
//
//    alert(result);
//}
